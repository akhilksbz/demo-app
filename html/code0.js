gdjs.MainScreenCode = {};
gdjs.MainScreenCode.GDStratButtonObjects1= [];
gdjs.MainScreenCode.GDStratButtonObjects2= [];
gdjs.MainScreenCode.GDStratButtonObjects3= [];
gdjs.MainScreenCode.GDBackgroundObjects1= [];
gdjs.MainScreenCode.GDBackgroundObjects2= [];
gdjs.MainScreenCode.GDBackgroundObjects3= [];
gdjs.MainScreenCode.GDSreenSp1Objects1= [];
gdjs.MainScreenCode.GDSreenSp1Objects2= [];
gdjs.MainScreenCode.GDSreenSp1Objects3= [];
gdjs.MainScreenCode.GDSreenSp2Objects1= [];
gdjs.MainScreenCode.GDSreenSp2Objects2= [];
gdjs.MainScreenCode.GDSreenSp2Objects3= [];
gdjs.MainScreenCode.GDSreenSp3Objects1= [];
gdjs.MainScreenCode.GDSreenSp3Objects2= [];
gdjs.MainScreenCode.GDSreenSp3Objects3= [];
gdjs.MainScreenCode.GDTitleObjects1= [];
gdjs.MainScreenCode.GDTitleObjects2= [];
gdjs.MainScreenCode.GDTitleObjects3= [];
gdjs.MainScreenCode.GDExitButtonObjects1= [];
gdjs.MainScreenCode.GDExitButtonObjects2= [];
gdjs.MainScreenCode.GDExitButtonObjects3= [];
gdjs.MainScreenCode.GDDigitCountObjects1= [];
gdjs.MainScreenCode.GDDigitCountObjects2= [];
gdjs.MainScreenCode.GDDigitCountObjects3= [];
gdjs.MainScreenCode.GDDigitactivatorObjects1= [];
gdjs.MainScreenCode.GDDigitactivatorObjects2= [];
gdjs.MainScreenCode.GDDigitactivatorObjects3= [];
gdjs.MainScreenCode.GDfadeObjects1= [];
gdjs.MainScreenCode.GDfadeObjects2= [];
gdjs.MainScreenCode.GDfadeObjects3= [];
gdjs.MainScreenCode.GDBoxObjects1= [];
gdjs.MainScreenCode.GDBoxObjects2= [];
gdjs.MainScreenCode.GDBoxObjects3= [];
gdjs.MainScreenCode.GDtextYesObjects1= [];
gdjs.MainScreenCode.GDtextYesObjects2= [];
gdjs.MainScreenCode.GDtextYesObjects3= [];
gdjs.MainScreenCode.GDtextNoObjects1= [];
gdjs.MainScreenCode.GDtextNoObjects2= [];
gdjs.MainScreenCode.GDtextNoObjects3= [];
gdjs.MainScreenCode.GDInfotextObjects1= [];
gdjs.MainScreenCode.GDInfotextObjects2= [];
gdjs.MainScreenCode.GDInfotextObjects3= [];

gdjs.MainScreenCode.conditionTrue_0 = {val:false};
gdjs.MainScreenCode.condition0IsTrue_0 = {val:false};
gdjs.MainScreenCode.condition1IsTrue_0 = {val:false};
gdjs.MainScreenCode.condition2IsTrue_0 = {val:false};
gdjs.MainScreenCode.conditionTrue_1 = {val:false};
gdjs.MainScreenCode.condition0IsTrue_1 = {val:false};
gdjs.MainScreenCode.condition1IsTrue_1 = {val:false};
gdjs.MainScreenCode.condition2IsTrue_1 = {val:false};


gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDStratButtonObjects1Objects = Hashtable.newFrom({"StratButton": gdjs.MainScreenCode.GDStratButtonObjects1});gdjs.MainScreenCode.eventsList0x728b74 = function(runtimeScene) {

{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDStratButtonObjects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDStratButtonObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDStratButtonObjects1[i].setAnimation(2);
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameScreen", false);
}}

}


}; //End of gdjs.MainScreenCode.eventsList0x728b74
gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDStratButtonObjects1Objects = Hashtable.newFrom({"StratButton": gdjs.MainScreenCode.GDStratButtonObjects1});gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDExitButtonObjects1Objects = Hashtable.newFrom({"ExitButton": gdjs.MainScreenCode.GDExitButtonObjects1});gdjs.MainScreenCode.eventsList0x729294 = function(runtimeScene) {

{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDExitButtonObjects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDExitButtonObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDExitButtonObjects1[i].setAnimation(2);
}
}{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}}

}


}; //End of gdjs.MainScreenCode.eventsList0x729294
gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDExitButtonObjects1Objects = Hashtable.newFrom({"ExitButton": gdjs.MainScreenCode.GDExitButtonObjects1});gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDDigitactivatorObjects1Objects = Hashtable.newFrom({"Digitactivator": gdjs.MainScreenCode.GDDigitactivatorObjects1});gdjs.MainScreenCode.eventsList0x729e2c = function(runtimeScene) {

{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 5;
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


}; //End of gdjs.MainScreenCode.eventsList0x729e2c
gdjs.MainScreenCode.eventsList0x729abc = function(runtimeScene) {

{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
}

}


{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}
{ //Subevents
gdjs.MainScreenCode.eventsList0x729e2c(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.MainScreenCode.eventsList0x729abc
gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDDigitactivatorObjects1Objects = Hashtable.newFrom({"Digitactivator": gdjs.MainScreenCode.GDDigitactivatorObjects1});gdjs.MainScreenCode.eventsList0x72a63c = function(runtimeScene) {

{


{
/* Reuse gdjs.MainScreenCode.GDBoxObjects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDBoxObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDBoxObjects1[i].getBehavior("Tween").addObjectOpacityTween("fede-in1", 200, "linear", 1000, false);
}
}}

}


}; //End of gdjs.MainScreenCode.eventsList0x72a63c
gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDtextYesObjects1Objects = Hashtable.newFrom({"textYes": gdjs.MainScreenCode.GDtextYesObjects1});gdjs.MainScreenCode.eventsList0x72aa7c = function(runtimeScene) {

{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.stopGame(runtimeScene);
}}

}


}; //End of gdjs.MainScreenCode.eventsList0x72aa7c
gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDtextNoObjects1Objects = Hashtable.newFrom({"textNo": gdjs.MainScreenCode.GDtextNoObjects1});gdjs.MainScreenCode.eventsList0x72adf4 = function(runtimeScene) {

{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).setNumber(0);
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "Layer4");
}}

}


}; //End of gdjs.MainScreenCode.eventsList0x72adf4
gdjs.MainScreenCode.eventsList0x5b7a18 = function(runtimeScene) {

{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
gdjs.MainScreenCode.GDDigitactivatorObjects1.createFrom(runtimeScene.getObjects("Digitactivator"));
gdjs.MainScreenCode.GDSreenSp1Objects1.createFrom(runtimeScene.getObjects("SreenSp1"));
gdjs.MainScreenCode.GDSreenSp3Objects1.createFrom(runtimeScene.getObjects("SreenSp3"));
gdjs.MainScreenCode.GDTitleObjects1.createFrom(runtimeScene.getObjects("Title"));
gdjs.MainScreenCode.GDfadeObjects1.createFrom(runtimeScene.getObjects("fade"));
{for(var i = 0, len = gdjs.MainScreenCode.GDDigitactivatorObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDDigitactivatorObjects1[i].hide();
}
}{gdjs.evtTools.camera.showLayer(runtimeScene, "fadeLayer");
}{for(var i = 0, len = gdjs.MainScreenCode.GDfadeObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDfadeObjects1[i].getBehavior("Tween").addObjectOpacityTween("Fade_in", 0, "linear", 1000, false);
}
}{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp1Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp1Objects1[i].getBehavior("Tween").addObjectPositionYTween("tw1", 80, "linear", 2000, false);
}
}{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp3Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp3Objects1[i].getBehavior("Tween").addObjectAngleTween("tw2", 5, "linear", 3000, false);
}
}{for(var i = 0, len = gdjs.MainScreenCode.GDTitleObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDTitleObjects1[i].getBehavior("Tween").addObjectScaleTween("tw11", 1.2, 1.2, "easeInOutExpo", 1000, false);
}
}{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp1Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp1Objects1[i].setAnimation(0);
}
}{gdjs.evtTools.camera.hideLayer(runtimeScene, "Layer4");
}{runtimeScene.getVariables().getFromIndex(0).setNumber(0);
}}

}


{

gdjs.MainScreenCode.GDSreenSp3Objects1.createFrom(runtimeScene.getObjects("SreenSp3"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainScreenCode.GDSreenSp3Objects1.length;i<l;++i) {
    if ( gdjs.MainScreenCode.GDSreenSp3Objects1[i].getBehavior("Tween").hasFinished("tw2") ) {
        gdjs.MainScreenCode.condition0IsTrue_0.val = true;
        gdjs.MainScreenCode.GDSreenSp3Objects1[k] = gdjs.MainScreenCode.GDSreenSp3Objects1[i];
        ++k;
    }
}
gdjs.MainScreenCode.GDSreenSp3Objects1.length = k;}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
{gdjs.MainScreenCode.conditionTrue_1 = gdjs.MainScreenCode.condition1IsTrue_0;
gdjs.MainScreenCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7504060);
}
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDSreenSp3Objects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp3Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp3Objects1[i].getBehavior("Tween").removeTween("tw2");
}
}{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp3Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp3Objects1[i].getBehavior("Tween").addObjectAngleTween("tw3", -(5), "linear", 3000, false);
}
}}

}


{

gdjs.MainScreenCode.GDSreenSp3Objects1.createFrom(runtimeScene.getObjects("SreenSp3"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainScreenCode.GDSreenSp3Objects1.length;i<l;++i) {
    if ( gdjs.MainScreenCode.GDSreenSp3Objects1[i].getBehavior("Tween").hasFinished("tw3") ) {
        gdjs.MainScreenCode.condition0IsTrue_0.val = true;
        gdjs.MainScreenCode.GDSreenSp3Objects1[k] = gdjs.MainScreenCode.GDSreenSp3Objects1[i];
        ++k;
    }
}
gdjs.MainScreenCode.GDSreenSp3Objects1.length = k;}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
{gdjs.MainScreenCode.conditionTrue_1 = gdjs.MainScreenCode.condition1IsTrue_0;
gdjs.MainScreenCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7504964);
}
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDSreenSp3Objects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp3Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp3Objects1[i].getBehavior("Tween").removeTween("tw3");
}
}{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp3Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp3Objects1[i].getBehavior("Tween").addObjectAngleTween("tw2", 5, "linear", 3000, false);
}
}}

}


{

gdjs.MainScreenCode.GDTitleObjects1.createFrom(runtimeScene.getObjects("Title"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainScreenCode.GDTitleObjects1.length;i<l;++i) {
    if ( gdjs.MainScreenCode.GDTitleObjects1[i].getBehavior("Tween").hasFinished("tw11") ) {
        gdjs.MainScreenCode.condition0IsTrue_0.val = true;
        gdjs.MainScreenCode.GDTitleObjects1[k] = gdjs.MainScreenCode.GDTitleObjects1[i];
        ++k;
    }
}
gdjs.MainScreenCode.GDTitleObjects1.length = k;}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
{gdjs.MainScreenCode.conditionTrue_1 = gdjs.MainScreenCode.condition1IsTrue_0;
gdjs.MainScreenCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7505884);
}
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDTitleObjects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDTitleObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDTitleObjects1[i].getBehavior("Tween").removeTween("tw11");
}
}{for(var i = 0, len = gdjs.MainScreenCode.GDTitleObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDTitleObjects1[i].getBehavior("Tween").addObjectScaleTween("tw11", 1, 1, "easeInOutExpo", 1000, false);
}
}}

}


{



}


{

gdjs.MainScreenCode.GDStratButtonObjects1.createFrom(runtimeScene.getObjects("StratButton"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDStratButtonObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
gdjs.MainScreenCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 0;
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDStratButtonObjects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDStratButtonObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDStratButtonObjects1[i].setAnimation(1);
}
}
{ //Subevents
gdjs.MainScreenCode.eventsList0x728b74(runtimeScene);} //End of subevents
}

}


{

gdjs.MainScreenCode.GDStratButtonObjects1.createFrom(runtimeScene.getObjects("StratButton"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDStratButtonObjects1Objects, runtimeScene, true, true);
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDStratButtonObjects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDStratButtonObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDStratButtonObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.MainScreenCode.GDExitButtonObjects1.createFrom(runtimeScene.getObjects("ExitButton"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDExitButtonObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
gdjs.MainScreenCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 0;
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDExitButtonObjects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDExitButtonObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDExitButtonObjects1[i].setAnimation(1);
}
}
{ //Subevents
gdjs.MainScreenCode.eventsList0x729294(runtimeScene);} //End of subevents
}

}


{

gdjs.MainScreenCode.GDExitButtonObjects1.createFrom(runtimeScene.getObjects("ExitButton"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDExitButtonObjects1Objects, runtimeScene, true, true);
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainScreenCode.GDExitButtonObjects1 */
{for(var i = 0, len = gdjs.MainScreenCode.GDExitButtonObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDExitButtonObjects1[i].setAnimation(0);
}
}}

}


{



}


{

gdjs.MainScreenCode.GDDigitactivatorObjects1.createFrom(runtimeScene.getObjects("Digitactivator"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDDigitactivatorObjects1Objects, runtimeScene, true, false);
}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
gdjs.MainScreenCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 0;
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {
gdjs.MainScreenCode.GDSreenSp2Objects1.createFrom(runtimeScene.getObjects("SreenSp2"));
{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp2Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp2Objects1[i].setAnimation(1);
}
}
{ //Subevents
gdjs.MainScreenCode.eventsList0x729abc(runtimeScene);} //End of subevents
}

}


{

gdjs.MainScreenCode.GDDigitactivatorObjects1.createFrom(runtimeScene.getObjects("Digitactivator"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDDigitactivatorObjects1Objects, runtimeScene, true, true);
}if (gdjs.MainScreenCode.condition0IsTrue_0.val) {
gdjs.MainScreenCode.GDSreenSp2Objects1.createFrom(runtimeScene.getObjects("SreenSp2"));
{for(var i = 0, len = gdjs.MainScreenCode.GDSreenSp2Objects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDSreenSp2Objects1[i].setAnimation(0);
}
}}

}


{


{
gdjs.MainScreenCode.GDDigitCountObjects1.createFrom(runtimeScene.getObjects("DigitCount"));
{for(var i = 0, len = gdjs.MainScreenCode.GDDigitCountObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDDigitCountObjects1[i].setString("level:" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}}

}


{



}


{


gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
{gdjs.MainScreenCode.conditionTrue_1 = gdjs.MainScreenCode.condition1IsTrue_0;
gdjs.MainScreenCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7513876);
}
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {
gdjs.MainScreenCode.GDBoxObjects1.createFrom(runtimeScene.getObjects("Box"));
{gdjs.evtTools.camera.showLayer(runtimeScene, "Layer4");
}{for(var i = 0, len = gdjs.MainScreenCode.GDBoxObjects1.length ;i < len;++i) {
    gdjs.MainScreenCode.GDBoxObjects1[i].setOpacity(0);
}
}
{ //Subevents
gdjs.MainScreenCode.eventsList0x72a63c(runtimeScene);} //End of subevents
}

}


{

gdjs.MainScreenCode.GDtextYesObjects1.createFrom(runtimeScene.getObjects("textYes"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
gdjs.MainScreenCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDtextYesObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.MainScreenCode.eventsList0x72aa7c(runtimeScene);} //End of subevents
}

}


{

gdjs.MainScreenCode.GDtextNoObjects1.createFrom(runtimeScene.getObjects("textNo"));

gdjs.MainScreenCode.condition0IsTrue_0.val = false;
gdjs.MainScreenCode.condition1IsTrue_0.val = false;
{
gdjs.MainScreenCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}if ( gdjs.MainScreenCode.condition0IsTrue_0.val ) {
{
gdjs.MainScreenCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MainScreenCode.mapOfGDgdjs_46MainScreenCode_46GDtextNoObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.MainScreenCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.MainScreenCode.eventsList0x72adf4(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.MainScreenCode.eventsList0x5b7a18


gdjs.MainScreenCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MainScreenCode.GDStratButtonObjects1.length = 0;
gdjs.MainScreenCode.GDStratButtonObjects2.length = 0;
gdjs.MainScreenCode.GDStratButtonObjects3.length = 0;
gdjs.MainScreenCode.GDBackgroundObjects1.length = 0;
gdjs.MainScreenCode.GDBackgroundObjects2.length = 0;
gdjs.MainScreenCode.GDBackgroundObjects3.length = 0;
gdjs.MainScreenCode.GDSreenSp1Objects1.length = 0;
gdjs.MainScreenCode.GDSreenSp1Objects2.length = 0;
gdjs.MainScreenCode.GDSreenSp1Objects3.length = 0;
gdjs.MainScreenCode.GDSreenSp2Objects1.length = 0;
gdjs.MainScreenCode.GDSreenSp2Objects2.length = 0;
gdjs.MainScreenCode.GDSreenSp2Objects3.length = 0;
gdjs.MainScreenCode.GDSreenSp3Objects1.length = 0;
gdjs.MainScreenCode.GDSreenSp3Objects2.length = 0;
gdjs.MainScreenCode.GDSreenSp3Objects3.length = 0;
gdjs.MainScreenCode.GDTitleObjects1.length = 0;
gdjs.MainScreenCode.GDTitleObjects2.length = 0;
gdjs.MainScreenCode.GDTitleObjects3.length = 0;
gdjs.MainScreenCode.GDExitButtonObjects1.length = 0;
gdjs.MainScreenCode.GDExitButtonObjects2.length = 0;
gdjs.MainScreenCode.GDExitButtonObjects3.length = 0;
gdjs.MainScreenCode.GDDigitCountObjects1.length = 0;
gdjs.MainScreenCode.GDDigitCountObjects2.length = 0;
gdjs.MainScreenCode.GDDigitCountObjects3.length = 0;
gdjs.MainScreenCode.GDDigitactivatorObjects1.length = 0;
gdjs.MainScreenCode.GDDigitactivatorObjects2.length = 0;
gdjs.MainScreenCode.GDDigitactivatorObjects3.length = 0;
gdjs.MainScreenCode.GDfadeObjects1.length = 0;
gdjs.MainScreenCode.GDfadeObjects2.length = 0;
gdjs.MainScreenCode.GDfadeObjects3.length = 0;
gdjs.MainScreenCode.GDBoxObjects1.length = 0;
gdjs.MainScreenCode.GDBoxObjects2.length = 0;
gdjs.MainScreenCode.GDBoxObjects3.length = 0;
gdjs.MainScreenCode.GDtextYesObjects1.length = 0;
gdjs.MainScreenCode.GDtextYesObjects2.length = 0;
gdjs.MainScreenCode.GDtextYesObjects3.length = 0;
gdjs.MainScreenCode.GDtextNoObjects1.length = 0;
gdjs.MainScreenCode.GDtextNoObjects2.length = 0;
gdjs.MainScreenCode.GDtextNoObjects3.length = 0;
gdjs.MainScreenCode.GDInfotextObjects1.length = 0;
gdjs.MainScreenCode.GDInfotextObjects2.length = 0;
gdjs.MainScreenCode.GDInfotextObjects3.length = 0;

gdjs.MainScreenCode.eventsList0x5b7a18(runtimeScene);
return;

}

gdjs['MainScreenCode'] = gdjs.MainScreenCode;
